<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'edit books']);
        Permission::create(['name' => 'delete books']);
        Permission::create(['name' => 'publish books']);
        
        $role1 = Role::create(['name' => 'bibliotekar']);
        $role1->givePermissionTo('publish books');
        $role1->givePermissionTo('delete books');
        $role1->givePermissionTo('edit books');

        $role2 = Role::create(['name' => 'pocnik bibliotekara']);
        $role2->givePermissionTo('edit books');
        
        $role3 = Role::create(['name' => 'super-admin']);

    }
}
