<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Book;
use App\Subcategory;

class Category extends Model
{
    public function books()
    {
        return $this->hasMany(Book::class);
    }

    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }
}
