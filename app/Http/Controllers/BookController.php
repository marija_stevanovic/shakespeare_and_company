<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
    /**
     * @param Book $book
     * @return BookResource
     */
    public function show(Book $book)
    {
        return $book;
    }
}
